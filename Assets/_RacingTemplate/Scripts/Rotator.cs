﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

	public int rotationX = 0;
	public int rotationY = 0;
	public int rotationZ = 0;

	// Update is called once per frame
	void Update () {

		transform.Rotate(new Vector3 (rotationX, rotationY, rotationZ) * Time.deltaTime);

	}
}
