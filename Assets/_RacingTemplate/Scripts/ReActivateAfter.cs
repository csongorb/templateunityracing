﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReActivateAfter : MonoBehaviour {

	public float waitInSeconds = 1.0F;
	public GameObject objectToReactivate;

	private float startTime;
	private bool timerIsOn = false;

	// Use this for initialization
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{

		if (timerIsOn == false)
		{
			if (objectToReactivate.activeSelf == false)
			{
				startTime = Time.time;
				timerIsOn = true;
			}
		}

		if (timerIsOn == true)
		{
			if(Time.time > startTime + waitInSeconds)
			{
				 objectToReactivate.SetActive(true);
				 timerIsOn = false;
			}
		}
	}
}
