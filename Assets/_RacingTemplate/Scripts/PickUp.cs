using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PickUp : MonoBehaviour {

    private Rigidbody rb;
	 private int count;

    public Text countText;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
		  count = 0;
        SetCountText();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag ("PickUp"))
        {
            other.gameObject.SetActive (false);
				count = count + 1;
				SetCountText();
        }
    }

	 void SetCountText()
	    {
	        countText.text = "Pick Ups: " + count.ToString ();
	    }
}
