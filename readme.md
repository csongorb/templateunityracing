# Template: Unity Racing

A basic template for **racing-like games** in **Unity**.

The template is made for an **introductory workshop / short game jam** about **game design and game development**, mainly to bring up (and answer some) questions around these topics.

But it can also be used for **whatever you like**!  

## Structure / What's inside?

### Folders

- `_RacingTemplate`
    - an example game with three tracks
        - all **our** prefabs / scripts / etc.
- other folders (inside the `Asset`-Folder)
    - all **external** assets
    - from Unity
        - [Standard Assets](https://www.assetstore.unity3d.com/en/#!/content/32351), incl.
            - Cameras
            - CrossPlatformInput
            - Effects
            - Environment
            - Fonts
            - ParticleSystems
            - Physics Materials
            - Prototyping
            - Utility
            - Vehicles
    - from Daniel Robnik
        - [Low Poly Styled Trees](https://www.assetstore.unity3d.com/en/#!/content/43103)
        - [Low Poly Styled Rocks](https://www.assetstore.unity3d.com/en/#!/content/43486)
    - [http://www.mayang.com/textures](http://www.mayang.com/textures)

### Prefabs / Scripts / etc.

We have tried to include **as many as possible / a lot of different prefabs / scripts / etc. for easy and fast prototyping of ideas**.   
Of course such a list can never be complete (look further below for further inspiration)!

- menu structure
- camera positioning
- racetrack prototyping
- basic object texturing
- basic particle effect design
- time tracking
- win- & lose-states
- trigger structure
- collectibles

## How to use it?

### In general

- **fork** this template into a new repository / project (or just download it!)
- **open** the project in Unity
- **mess around** with whatever you like!

### In a Game-Jam-like environment

- take a in-dept look at the template
    - what are the possibilities? which ideas could be easily made?
- brainstorm
- develop
- finish!
- let other people play!

### Ideas

- Parking Game
- Storytelling Game (through triggers)
- Micro Machines
- Takedown
- Destruction Derby
- etc.

### Tutorials

- [Unity Official Tutorials](https://unity3d.com/learn/tutorials)
    - [Main Menu Structure](https://unity3d.com/learn/tutorials/modules/beginner/live-training-archive/creating-a-scene-menu)
    - [Collectible Objects](https://unity3d.com/learn/tutorials/projects/roll-ball-tutorial/creating-collectable-objects)
- just google Unity & Tutorials :-)

## Credits

**Great thanks** to all the **generous people and companies**, who are providing their ressources and assets to this template! Take a look above (structure) for further details!

## Licence

[![Creative Commons License](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc/4.0/)
This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/).

All other media (images, software, etc.) remain the property of their copyright holders.

## Changelog

### Todos / Bugs / Ideas

- Bugs
    - loading from the menu, the scenes get darker, why?
- Todos / Ideas
    - moveable camera (drone?)
    - collectibles with an effect (boost)
    - shooting
    - VR

### Work in Progress

- Updated Unity version to 2021.3.5f1 (Latest LTS release)
- Replaced Bolt with Visual Scripting (1.7.8)

### Version 0.4 (May 2022)

- Updated Unity version to 2021.3.1f1 (Latest LTS release)

### Version 0.3 (March 2022)

- Updated Unity version to 2020.3.30f1 (Latest LTS release)
- Imported and setup Bolt Visual Scripting tool
- Updated obsolete APIs
- Updated GitIgnore
- Imported TextMeshPro

### Version 0.2 (June 2017)

- updated to
    - Unity 5.5.2
    - Standard Assets 1.1.2
    - collectibles


### Version 0.1 (May 2016)

- first release
    - mainly for *Game Design in a Day 2017* at *BTK Berlin*
- two template scenes
    - standard racing track, incl. timer
    - linear track with storytelling structure (trigger)
- menu
